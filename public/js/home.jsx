'use strict';

var projects_list = [
    {title: "Irrational password generator", link: "/pwgenir"}
];

var Projects = React.createClass({
    render: function() {
        var ts = this.props.projects.map(function(item, i) {
            return <li key={i}><a href={item.link}>{item.title}</a></li>;
        });
        return <ul>{ts}</ul>;
    }
});

var Home = React.createClass({
    render: function() {
        return (
            <span>
                <div>Homepage</div>
                <h2>Projects</h2>
                <Projects projects={projects_list} />
            </span>
        );
    }
});

ReactDOM.render(<Home />, document.body);
